# Define base image.
FROM docker.io/library/python:slim AS run

# Configure working directory.
WORKDIR /app

# Add requirements file.
COPY requirements.txt .

# Install pyserial.
RUN pip install -r requirements.txt

# Download rfc2217 server source code.
COPY rfc2217_server.py .

# Make application executable.
RUN chmod +x rfc2217_server.py

# Set up environment variable to specify
ENV SERIALPORT=/dev/ttyUSB0
ENV TCPPORT=2217

# Expose port.
EXPOSE $TCPPORT

# Configure startup command for container.
CMD python rfc2217_server.py $SERIALPORT -p $TCPPORT
