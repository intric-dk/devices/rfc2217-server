# RFC2217 Server

A simple server to expose a serial port via a telnet connection.

## Usage

To use the application, simply run the following command:

```shell
$ docker run -d --restart always --privileged -v /dev:/dev -e SERIALPORT=/dev/ttyUSB0 -e TCPPORT=2217 -p 2217:2217 --name rfc2217-2217 registry.gitlab.com/intric-dk/devices/rfc2217-server
```

You can configure the application via the following environment variables:

- `SERIALPORT`: Specify the serial port you want to use (default: `/dev/ttyUSB0`).
- `TCPPORT`: Specify the TCP port you want to use (default: `2217`).

## License

This project is licensed under the terms of the [MIT license](./LICENSE).
